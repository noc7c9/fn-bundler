use clap::{App, Arg};

pub fn build<'a>() -> clap::App<'a, 'a> {
    App::new(crate_name!())
        .version(crate_version!())
        .about(crate_description!())
        .author(crate_authors!())
        .arg(Arg::with_name("output").help("The name of the output bundle file"))
        .arg(
            Arg::with_name("project-root")
                .short("r")
                .long("root")
                .help("Path to the root of the project")
                .default_value("."),
        )
}
