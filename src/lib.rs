#![warn(
    rust_2018_idioms,
    macro_use_extern_crate,
    missing_copy_implementations,
    missing_debug_implementations,
    missing_doc_code_examples,
    // missing_docs,
    single_use_lifetimes,
    trivial_casts,
    trivial_numeric_casts,
    unreachable_pub,
    unsafe_code,
    unused_labels,
    unused_lifetimes,
    unused_qualifications,
    variant_size_differences
)]

#[macro_use]
extern crate clap;
#[macro_use]
extern crate lazy_static;

mod arg_parser;
mod node_modules;
mod serverless_config;

use self::serverless_config::ServerlessConfig;
use globset::{Glob, GlobSet, GlobSetBuilder};
use same_file::is_same_file;
use std::fs::File;
use std::io::{Read, Write};
use std::path::Path;
use walkdir::WalkDir;

lazy_static! {
    static ref ZIP_OPTIONS: zip::write::FileOptions =
        zip::write::FileOptions::default().compression_method(zip::CompressionMethod::Deflated);
    // Patterns for files/folder that shouldn't be added to the bundle wherever
    // they appear
    static ref GLOBALLY_IGNORED: GlobSet = {
        let mut globset = GlobSetBuilder::new();

        globset.add(Glob::new(".git").unwrap());
        globset.add(Glob::new(".serverless").unwrap());
        globset.add(Glob::new("serverless.yml").unwrap());
        globset.add(Glob::new("serverless.yaml").unwrap());
        globset.add(Glob::new("yarn.lock").unwrap());

        globset.build().unwrap()
    };
}

pub fn run(args: Option<&[&str]>) {
    let arg_parser = arg_parser::build();
    let matches = match args {
        Some(args) => arg_parser.get_matches_from(args),
        None => arg_parser.get_matches(),
    };

    let output_file = matches.value_of("output");
    let root_path = matches.value_of("project-root").unwrap();
    create_bundle(root_path, output_file);
}

fn create_bundle(root_path: &str, output_file: Option<&str>) {
    println!("Parsing serverless config");
    let serverless_config = serverless_config::parse_config_in(root_path);

    let root_path = Path::new(root_path);

    let output_file = output_file
        .map_or_else(
            || {
                serverless_config
                    .artifact()
                    // the output filepath from the serverless config file is
                    // relative to the config file itself
                    // FIXME: joining to root_path works for now
                    .map(|artifact| root_path.join(artifact))
            },
            |s| Some(Path::new(s).into()),
        )
        .expect("Output file path not specified");

    // Ensure the output file's parent directories exist
    if let Some(output_file_parent) = Path::new(&output_file).parent() {
        std::fs::create_dir_all(output_file_parent).expect("Failed to create output file parents");
    }

    let writer = File::create(&output_file).unwrap();
    let mut writer = zip::ZipWriter::new(writer);

    let (blacklist, whitelist) = build_globsets(&serverless_config);

    println!("Adding source files to bundle");
    // Add source files based on any globbing rules if any
    let mut walker = WalkDir::new(root_path).into_iter();
    let mut buffer = Vec::with_capacity(1024 * 1024);
    loop {
        let entry = match walker.next() {
            None => break,
            Some(entry) => entry.unwrap(),
        };

        // Completely skip over node_modules
        if entry.file_name() == "node_modules" {
            walker.skip_current_dir();
            continue;
        }

        let path = entry.path();

        let rel_path = path.strip_prefix(root_path).unwrap();

        // Skip over globally ignored files and folders
        if GLOBALLY_IGNORED.is_match(rel_path) {
            if path.is_dir() {
                walker.skip_current_dir();
            }
            continue;
        }

        // don't include if its in the blacklist and not in the whitelist
        // ie. blacklist applied first and whitelist second
        if blacklist.is_match(rel_path) && !whitelist.is_match(rel_path) {
            continue;
        }

        if !path.is_file() {
            continue;
        }

        if is_same_file(path, &output_file).unwrap_or(false) {
            continue;
        }

        add_file_to_zip(&path, &root_path, &mut writer, &mut buffer);
    }

    println!("Adding node modules to bundle");
    // Add production node modules
    add_node_modules_to_bundle(&root_path, &root_path, &mut writer, &mut buffer);

    writer.finish().unwrap();
}

fn add_node_modules_to_bundle(
    parent_path: &Path,
    root_path: &Path,
    writer: &mut zip::ZipWriter<std::fs::File>,
    buffer: &mut Vec<u8>,
) {
    // Find the yarn lock and package.json files
    let package_json = parent_path.join("package.json");
    let yarn_lock = parent_path.join("yarn.lock");
    if let Ok(package_json) = std::fs::read_to_string(package_json) {
        if let Ok(yarn_lock) = std::fs::read_to_string(yarn_lock) {
            // Parse both files
            let dependencies = node_modules::parse_package_json(&package_json);
            let dependency_tree = node_modules::parse_yarn_lock(&yarn_lock);
            let full_dependency_list =
                node_modules::determine_full_dependency_list(&dependencies, &dependency_tree);
            let full_dependency_count = full_dependency_list.len();

            // Add the required dependencies to the bundle
            let node_modules_path = parent_path.join("node_modules");
            for (i, dependency) in full_dependency_list.iter().enumerate() {
                println!(
                    "Adding module: {} ({} of {})",
                    dependency,
                    i + 1,
                    full_dependency_count
                );

                let module_path = node_modules_path.join(dependency);
                let module_is_symlink = is_symlink(&module_path).unwrap_or(false);

                let mut walker = WalkDir::new(&module_path).into_iter();
                loop {
                    let entry = match walker.next() {
                        None => break,
                        Some(Err(_)) => continue,
                        Some(Ok(entry)) => entry,
                    };

                    let path = entry.path();

                    // If the module is a symlink, handle contents more carefully
                    if module_is_symlink {
                        // Recurse into any node_modules instead of adding it directly
                        if entry.file_name() == "node_modules" {
                            add_node_modules_to_bundle(&module_path, root_path, writer, buffer);
                            walker.skip_current_dir();
                            continue;
                        }

                        // Skip over globally ignored files and folders
                        if GLOBALLY_IGNORED.is_match(path.strip_prefix(&module_path).unwrap()) {
                            if path.is_dir() {
                                walker.skip_current_dir();
                            }
                            continue;
                        }
                    }

                    if !path.is_file() {
                        continue;
                    }

                    add_file_to_zip(&path, &root_path, writer, buffer);
                }
            }
        }
    }
}

fn add_file_to_zip(
    path: &Path,
    root_path: &Path,
    writer: &mut zip::ZipWriter<std::fs::File>,
    buffer: &mut Vec<u8>,
) {
    let name = path.strip_prefix(root_path).unwrap().to_str().unwrap();
    let name = if cfg!(windows) {
        name.replace("\\", "/")
    } else {
        name.to_string()
    };

    writer.start_file(name, *ZIP_OPTIONS).unwrap();
    let mut file = File::open(path).unwrap();
    file.read_to_end(buffer).unwrap();
    writer.write_all(buffer).unwrap();
    buffer.clear();
}

fn is_symlink(path: &Path) -> Result<bool, std::io::Error> {
    let metadata = std::fs::symlink_metadata(path)?;
    let is_symlink = metadata.file_type().is_symlink();
    Ok(is_symlink)
}

fn build_globsets(serverless_config: &ServerlessConfig) -> (GlobSet, GlobSet) {
    // add the patterns from the serverless config

    let mut blacklist = GlobSetBuilder::new();
    add_patterns(&mut blacklist, serverless_config.exclude_patterns());

    let mut whitelist = GlobSetBuilder::new();
    add_patterns(&mut whitelist, serverless_config.include_patterns());

    (blacklist.build().unwrap(), whitelist.build().unwrap())
}

fn add_patterns(builder: &mut GlobSetBuilder, patterns: &[String]) {
    for pattern in patterns {
        if let Ok(glob) = Glob::new(&pattern) {
            builder.add(glob);
        }
    }
}
