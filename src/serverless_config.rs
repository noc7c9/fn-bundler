use std::path::PathBuf;
use yaml_rust::{Yaml, YamlLoader};

#[derive(Default)]
pub struct ServerlessConfig {
    artifact: Option<String>,
    exclude_patterns: Vec<String>,
    include_patterns: Vec<String>,
}

impl ServerlessConfig {
    pub fn artifact(&self) -> Option<String> {
        self.artifact.clone()
    }
    pub fn exclude_patterns(&self) -> &Vec<String> {
        &self.exclude_patterns
    }
    pub fn include_patterns(&self) -> &Vec<String> {
        &self.include_patterns
    }
}

pub fn parse_config_in(project_path: &str) -> ServerlessConfig {
    let mut project_path = PathBuf::from(project_path);
    find_config(&mut project_path)
        .and_then(|file| std::fs::read_to_string(file).ok())
        .map(|data| parse_str(&data))
        .unwrap_or_else(Default::default)
}

fn parse_str(config: &str) -> ServerlessConfig {
    let config = YamlLoader::load_from_str(config).unwrap();
    let package = &config[0]["package"];

    let artifact = package["artifact"].as_str().map(|s| s.to_string());
    let exclude = package["exclude"].as_vec();
    let include = package["include"].as_vec();

    ServerlessConfig {
        artifact,
        exclude_patterns: exclude.map_or_else(Default::default, yaml_list_to_string_vec),
        include_patterns: include.map_or_else(Default::default, yaml_list_to_string_vec),
    }
}

fn yaml_list_to_string_vec(vec: &Vec<Yaml>) -> Vec<String> {
    vec.iter()
        .filter_map(|pattern| pattern.as_str().map(|s| s.to_string()))
        .collect()
}

fn find_config(project_path: &mut PathBuf) -> Option<&str> {
    project_path.push("serverless.yml");
    let path = project_path;
    if path.exists() {
        return path.to_str();
    }
    path.set_extension("yaml");
    if path.exists() {
        return path.to_str();
    }
    None
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn parsing_serverless_config_without_any_package_blocks() {
        let raw_config = "
service: serv
provider:
    name: aws
";
        let config = parse_str(raw_config);
        assert_eq!(config.artifact(), None);
        assert!(config.include_patterns().is_empty());
        assert!(config.exclude_patterns().is_empty());
    }

    #[test]
    fn parsing_serverless_config_with_artifact_in_package_block() {
        let raw_config = "
service: serv
provider:
    name: aws
package:
    artifact: bundle.zip
";
        let config = parse_str(raw_config);
        assert_eq!(config.artifact(), Some("bundle.zip".to_string()));
        assert!(config.include_patterns().is_empty());
        assert!(config.exclude_patterns().is_empty());
    }

    #[test]
    fn parsing_serverless_config_with_only_exclude_block_in_package_block() {
        let raw_config = "
service: serv
provider:
    name: aws
package:
    artifact: fn.zip
    exclude:
      - docs/**
      - .gitignore
      - \"**\"
";
        let config = parse_str(raw_config);

        assert!(config.include_patterns().is_empty());

        let exclude_patterns = config.exclude_patterns();
        assert!(exclude_patterns.contains(&"docs/**".to_string()));
        assert!(exclude_patterns.contains(&".gitignore".to_string()));
        assert!(exclude_patterns.contains(&"**".to_string()));
    }

    #[test]
    fn parsing_serverless_config_with_only_include_block_in_package_block() {
        let raw_config = "
service: serv
provider:
    name: aws
package:
    artifact: fn.zip
    include:
      - src/**
      - handler.js
";
        let config = parse_str(raw_config);

        let include_patterns = config.include_patterns();
        assert!(include_patterns.contains(&"src/**".to_string()));
        assert!(include_patterns.contains(&"handler.js".to_string()));

        assert!(config.exclude_patterns().is_empty());
    }

    #[test]
    fn parsing_serverless_config_with_both_exclude_and_include_blocks_in_package_block() {
        let raw_config = "
service: serv
provider:
    name: aws
package:
    artifact: fn.zip
    exclude:
      - docs/**
      - .gitignore
    include:
      - src/**
      - handler.js
";
        let config = parse_str(raw_config);

        let include_patterns = config.include_patterns();
        assert!(include_patterns.contains(&"src/**".to_string()));
        assert!(include_patterns.contains(&"handler.js".to_string()));

        let exclude_patterns = config.exclude_patterns();
        assert!(exclude_patterns.contains(&"docs/**".to_string()));
        assert!(exclude_patterns.contains(&".gitignore".to_string()));
    }

    #[test]
    fn parsing_serverless_config_with_invalid_keys_in_the_exclude_block() {
        let raw_config = "
service: serv
provider:
    name: aws
package:
    artifact: fn.zip
    exclude:
      - valid
      - not: valid
      - also-valid
";
        let config = parse_str(raw_config);

        let exclude_patterns = config.exclude_patterns();
        assert!(exclude_patterns.contains(&"valid".to_string()));
        assert!(exclude_patterns.contains(&"also-valid".to_string()));
    }

    #[test]
    fn parsing_serverless_config_with_invalid_keys_in_the_include_block() {
        let raw_config = "
service: serv
provider:
    name: aws
package:
    artifact: fn.zip
    include:
      - valid
      - not: valid
      - also-valid
";
        let config = parse_str(raw_config);

        let include_patterns = config.include_patterns();
        assert!(include_patterns.contains(&"valid".to_string()));
        assert!(include_patterns.contains(&"also-valid".to_string()));
    }

}
