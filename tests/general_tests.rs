mod utils;

bundle_test!(
    project_with_only_source_code,
    [
        "index.js",
        "src/lib.js",
        "src/utils/one.js",
        "src/utils/two.js",
    ]
);

bundle_test!(
    project_with_git,
    [
        "index.js",
        "src/lib.js",
        "src/utils/one.js",
        "src/utils/two.js",
    ]
);

#[test]
fn bundling_excludes_the_output_file() {
    use self::utils::assert_zip_contents;

    let testdir = "./tests/fixtures/bundling_excludes_the_output_file";
    let zipfile = "./tests/fixtures/bundling_excludes_the_output_file/output.zip";
    fn_bundler::run(Some(&["bin", "--root", testdir, zipfile]));

    teardown!(|| {
        std::fs::remove_file(zipfile).unwrap();
    });

    assert_zip_contents(zipfile, &mut ["index.js", "excluded/included/index.js"]);
}
