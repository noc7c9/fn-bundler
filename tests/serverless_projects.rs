mod utils;

bundle_test!(
    serverless_project_without_patterns,
    [
        "index.js",
        "src/lib.js",
        "src/utils/one.js",
        "src/utils/two.js",
    ]
);

bundle_test!(
    serverless_project_with_only_exclude_patterns,
    [
        "index.js",
        "src/lib.js",
        "src/utils/one.js",
        "src/utils/two.js",
    ]
);

bundle_test!(
    serverless_project_with_exclude_and_include_patterns,
    [
        "index.js",
        "src/common.js",
        "src/lambda/handler.js",
        "src/lambda/lib.js",
    ]
);

bundle_test!(
    serverless_project_with_exclude_all_pattern,
    [
        "index.js",
        "src/common.js",
        "src/lambda/handler.js",
        "src/lambda/lib.js",
    ]
);

#[test]
fn serverless_project_with_artifact_field() {
    use self::utils::assert_zip_contents;

    let artifact = "./tests/fixtures/serverless_project_with_artifact_field/artifact.zip";

    let testdir = "./tests/fixtures/serverless_project_with_artifact_field";
    fn_bundler::run(Some(&["bin", "--root", testdir]));

    teardown!(|| {
        std::fs::remove_file(artifact).unwrap();
    });

    assert_zip_contents(
        artifact,
        &mut [
            "index.js",
            "src/lib.js",
            "src/utils/one.js",
            "src/utils/two.js",
        ],
    );
}

#[test]
fn serverless_project_with_artifact_field_with_missing_parent_folders() {
    use self::utils::assert_zip_contents;

    let artifact = "./tests/fixtures/serverless_project_with_artifact_field_with_missing_parent_folders/build/artifact.zip";

    let testdir =
        "./tests/fixtures/serverless_project_with_artifact_field_with_missing_parent_folders";
    fn_bundler::run(Some(&["bin", "--root", testdir]));

    teardown!(|| {
        std::fs::remove_file(artifact).unwrap();
    });

    assert_zip_contents(
        artifact,
        &mut [
            "index.js",
            "src/lib.js",
            "src/utils/one.js",
            "src/utils/two.js",
        ],
    );
}
