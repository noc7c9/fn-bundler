use pretty_assertions::assert_eq;
use std::{fmt, fs, path};

fn collect_zip_file_list(zip_filepath: &str) -> Vec<String> {
    let path = path::Path::new(zip_filepath);
    let file = fs::File::open(&path).unwrap();
    let mut archive = zip::ZipArchive::new(file).unwrap();

    let file_count = archive.len();
    let mut file_list = Vec::with_capacity(file_count);
    for i in 0..file_count {
        let file = archive.by_index(i).unwrap();
        let filename = file.name().to_string();
        if !filename.ends_with('/') {
            file_list.push(filename);
        }
    }

    file_list
}

// A newtype around Vec to provide a nicer output for assert_zip_contents via
// pretty_assertions
#[derive(PartialEq)]
struct VecPrettyAssertWrapper<'a>(&'a [&'a str]);

impl<'a> fmt::Debug for VecPrettyAssertWrapper<'a> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(
            fmt,
            "{}",
            self.0
                .iter()
                .map(|s| format!("./{}", s))
                .collect::<Vec<String>>()
                .join("\n")
        )
    }
}

pub fn assert_zip_contents(zipfile: &str, expected_contents: &mut [&str]) {
    let mut zip_contents = collect_zip_file_list(zipfile);

    zip_contents.sort_unstable();
    expected_contents.sort_unstable();

    let zip_contents: Vec<&str> = zip_contents.iter().map(String::as_ref).collect();

    assert_eq!(
        VecPrettyAssertWrapper(expected_contents),
        VecPrettyAssertWrapper(&zip_contents)
    )
}

#[macro_export]
macro_rules! bundle_test {
    ($name: ident, $expected: expr) => {
        #[test]
        fn $name() {
            let tempdir = tempfile::Builder::new()
                .prefix("fn-bundler-test-")
                .tempdir()
                .expect("Unable to make tempdir for test");
            let zipfile = tempdir.path().join("bundle.zip");
            let zipfile = zipfile.to_str().unwrap();

            let testdir = concat!("./tests/fixtures/", stringify!($name));
            fn_bundler::run(Some(&["bin", "--root", testdir, zipfile]));

            utils::assert_zip_contents(zipfile, &mut $expected);
        }
    };
}

#[macro_export]
macro_rules! teardown {
    ($closure: expr) => {
        let _finally = crate::utils::Teardown {
            teardown: Some($closure),
        };
    };
}

#[allow(dead_code)]
pub struct Teardown<T>
where
    T: FnOnce() -> (),
{
    pub teardown: Option<T>,
}

impl<T> Drop for Teardown<T>
where
    T: FnOnce() -> (),
{
    fn drop(&mut self) {
        if let Some(teardown) = self.teardown.take() {
            teardown();
        }
    }
}
